# pylint: disable=line-too-long

'''
scraper.py contains various methods for scraping
HTML content and packaging into dictionaries.
'''

import requests
from bs4 import BeautifulSoup

def fetchHTML(url):
    '''
    Returns HTML retrived from a url.

    Parameters:
        url: String
            - The URL to fetch HTML from

    Returns:
        html: String
            - The HTML from a given URL
    '''
    req = requests.get(url)
    html = req.text
    return html

def extractTag(content, tag, className=None):
    '''
    Returns data embed within a tag, along
    with an optional class for filtering.

    Parameters:
        content: String
            - The HTML to parse
        tag: String
            - The HTML tag to scan for
        class: String
            - Optional filter for tag

    Returns:
        filteredData: List
            - Content embed within searched tags
    '''
    soup = BeautifulSoup(content)
    data = soup.findAll(tag, {'class': className})
    filteredData = []
    for datum in data:
        filteredData.append(datum.text.replace('Rs.','').strip().encode('utf-8'))
    return filteredData

def extractFromClass(content, className=None):
    soup = BeautifulSoup(content)
    data = soup.findAll(class_ = className)
    filteredData = []
    for datum in data:
        filteredData.append(datum.text.replace('Rs.','').strip().encode('utf-8'))
    return filteredData

def flipkartData(query,key):
    url = "http://www.flipkart.com/search?q="+query
    content = fetchHTML(url)
    soup = BeautifulSoup(content)
    product_items = soup.select('.product-unit')
    data = []
    for i in range(len(product_items)):
        if i == 20:
            break
        data.append({
        'image': str(product_items[i].find_all('img')[0]['data-src'].strip()),
        'title': str(product_items[i].find_all(class_ = 'pu-title')[0].a.text.strip().encode('utf-8')),
        'url': str("http://www.flipkart.com"+product_items[i].find_all(class_ = 'pu-title')[0].a['href'].encode('utf-8')),
        'price': int(product_items[i].find_all(class_ = 'fk-font-17')[0].text.replace('Rs.','').replace(",","").strip().encode('utf-8')),
        'stars': round(float(product_items[i].find_all(class_ = 'rating')[0]['style'].replace('width:','').replace('%;',''))*5/100,1) if product_items[i].find_all(class_ = 'rating') else "",
        'rating': int(product_items[i].find_all(class_ = 'pu-rating')[0].text.replace("(","").replace(")","").replace(",","").replace("ratings","").replace("rating","").strip().encode('utf-8')) if product_items[i].find_all(class_ = 'pu-rating') else ""
        })

    if key=='':
       return data
    elif key=='min':
       newlist = sorted(data, key=lambda k: k['price'])
       return newlist[0]
    elif key=='max':
       newlist = sorted(data,key=lambda k: k['price'],reverse=True)
       return newlist[0]

def AmazonData(query,key):
    url = "http://www.amazon.in/s/field-keywords="+query
    content = fetchHTML(url)
    soup = BeautifulSoup(content)
    product_items = soup.select('.s-item-container .a-fixed-left-grid')
    data = []
    for i in range(len(product_items)):
        if i == 20:
            break
        data.append({
        'image': str(product_items[i].find_all('img')[0]['src'].strip()),
        'title': str(product_items[i].find_all(class_ = 's-access-title')[0].text.strip().encode('utf-8').replace("\xc2\xa0", "")),
        'url': str(product_items[i].find_all(class_ = 's-access-detail-page')[0]['href']),
        'price': str(product_items[i].find_all(class_ = 'a-color-price')[0].text.replace('.00','').replace(",","").strip().encode('utf-8').replace("\xc2\xa0", "")),
        'stars': float(product_items[i].find_all(class_ = 'a-icon-star')[0].text.replace("out of 5 stars","").strip().encode('utf-8')) if product_items[i].find_all(class_ = 'a-icon-star') else "",
        'rating': int(product_items[i].find_all(class_ = 'a-span-last')[0].select('a')[1].text.replace(",","")) if product_items[i].find_all(class_ = 'a-icon-star') else "",
        })
    if key=='':
       return data
    elif key=='min':
       newlist = sorted(data, key=lambda k: k['price'])
       return newlist[0]
    elif key=='max':
       newlist = sorted(data,key=lambda k: k['price'],reverse=True)
       return newlist[0]

def SnapdealData(query,key):
    url = "http://www.snapdeal.com/search?keyword="+query+"&sort=rlvncy"
    content = fetchHTML(url)
    soup = BeautifulSoup(content)
    product_items = soup.select('.product-tuple-listing')
    data = []
    for i in range(len(product_items)):
        if i == 20:
            break
        data.append({
        'image': str(product_items[i].find_all(class_ = 'product-tuple-image')[0].a.img.get('src')) if product_items[i].find_all(class_ = 'product-tuple-image')[0].a.img.get('src') else str(product_items[i].find_all(class_ = 'product-tuple-image')[0].a.img.get('lazysrc')),
        'title': str(product_items[i].find_all(class_ = 'product-title')[0].text.strip().encode('utf-8')),
        'url': str(product_items[i].find_all(class_ = 'product-tuple-description')[0].a['href']) if product_items[i].find_all(class_ = 'product-tuple-description') else "",
        'price': int(product_items[i].find_all(class_ = 'product-price')[0].text.replace("Rs.","").replace(",","").strip()) if product_items[i].find_all(class_ = 'product-price') else "",
        'stars': round(float(product_items[i].find_all(class_ = 'filled-stars')[0]['style'].replace('width:','').replace('%',''))*5/100,1) if product_items[i].find_all(class_ = 'filled-stars') else "",
        'rating': int(product_items[i].find_all(class_ = 'product-rating-count')[0].text.replace("(","").replace(")","")) if product_items[i].find_all(class_ = 'product-rating-count') else "",
        })
    if key=='':
       return data
    elif key=='min':
       newlist = sorted(data, key=lambda k: k['price'])
       return newlist[0]
    elif key=='max':
       newlist = sorted(data,key=lambda k: k['price'],reverse=True)
       return newlist[0]


def steamDiscounts():
    '''Returns discounts from steam.com'''
    req = requests.get('http://store.steampowered.com/search/?specials=1#sort_by=_ASC&sort_order=ASC&specials=1&page=1')
    content = req.text
    soup = BeautifulSoup(content)
    allData = {id: {} for id in range(0, 25)}

    # Get all divs of a specific class
    releaseDate = soup.findAll('div', {'class': 'col search_released'})

    # Get all release dates
    releaseDates = []
    id = 0
    for date in releaseDate:
        allData[id]['releaseDates'] = date.text
        releaseDates.append(date.text)
        id += 1

    #print releaseDates

    id = 0
    gameName = soup.findAll('div', {'class': 'col search_name ellipsis'})

    # Get all game names
    gameNames = []
    for name in gameName:
        span = name.findAll('span', {'class': 'title'})
        for tag in span:
            allData[id]['name'] = tag.text
            gameNames.append(tag.text)
            id += 1

    # print gameNames

    discount = soup.findAll('div', {'class': 'col search_discount'})

    id = 0
    # Get all game discounts
    gameDiscounts = []
    for discountedGame in discount:
        span = discountedGame.findAll('span')
        for tag in span:
            allData[id]['discount'] = tag.text
            gameDiscounts.append(tag.text)
            id += 1

    # print gameDiscounts

    price = soup.findAll('div', {'class': 'col search_price discounted'})

    id = 0
    prices = []
    # Get all discounted prices
    for value in price:
        br = value.findAll('br')
        for tag in br:
            allData[id]['price'] = tag.text.strip('\t')
            prices.append(tag.text.strip('\t'))
            id += 1

    # Cleanup data
    newData = []
    for datum in allData:
        newData.append(allData[datum])
    # print prices
    return newData

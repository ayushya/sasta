window.sort  = false;
$("#search-form").on("submit",function(e){
  e.preventDefault();
  $(".loading-spinner").removeClass("hide");
  $(".load").removeClass("hide");
  $(".data-content").fadeTo(500,0.5);
  $('.search').animate({ marginTop: '50px' }, 2000, function(){
    $("#search-results").removeClass("hide");
    $(".loading-spinner").remove();
  });

var maxResults = 20, i;
  //Flipkart
  $.get(location.origin+"/search/flipkart/"+$("#squery").val(),function(data,status,jqXHR){
    if(status === "success") {
      // console.log(data);
      var flipkartObj = sort ? _.sortBy(data.flipkart, function(o){return o['price']}) : data.flipkart,
      $flipkart_data_blk = $("#search-results .flipkart-blk .data-content");
      $flipkart_data_blk.html("");
      for(i=0;i<flipkartObj.length&&i<maxResults;i++) {
        $flipkart_data_blk.append('\
          <div class="row item-unit mb2">\
            <div class="columns medium-2 small-2 p1 text-center img-blk ml2">\
              <a target="_blank" href='+flipkartObj[i]['url']+'><img src='+flipkartObj[i]['image']+' /></a>\
            </div>\
            <div class="columns medium-9 small-9 pl0 info-blk">\
              <h5 class="mt1"><a target="_blank" href='+flipkartObj[i]['url']+'>'+flipkartObj[i]['title']+'</a></h5>\
              <span title="'+flipkartObj[i]['rating']+' Reviews & Ratings" class="stars-container mt1 stars-'+(parseInt(flipkartObj[i]['stars']*2)*10).toString()+' ml3">★★★★★</span>\
              <a target="_blank" href='+flipkartObj[i]['url']+'><button class="button success mb1 price-btn"><strong>&#8377; '+flipkartObj[i]['price']+'</strong></button></a>\
            </div>\
          </div>\
        ');
      }
      $(".flipkart-blk .load").addClass("hide");
      $flipkart_data_blk.fadeTo(500,1);
    }
  });

  //Amazon
  $.get(location.origin+"/search/amazon/"+$("#squery").val(),function(data,status,jqXHR){
    if(status === "success") {
      // console.log(data);
      var amazonObj = sort ? _.sortBy(data.amazon, function(o){return o['price']}) : data.amazon,
      $amazon_data_blk = $("#search-results .amazon-blk .data-content");
      $amazon_data_blk.html("");
      for(i=0;i<amazonObj.length&&i<maxResults;i++) {
        $amazon_data_blk.append('\
          <div class="row item-unit mb2">\
            <div class="columns medium-2 small-2 p1 text-center img-blk ml2">\
              <a target="_blank" href='+amazonObj[i]['url']+'><img src='+amazonObj[i]['image']+' /></a>\
            </div>\
            <div class="columns medium-9 small-9 pl0 info-blk">\
              <h5 class="mt1"><a target="_blank" href='+amazonObj[i]['url']+'>'+amazonObj[i]['title']+'</a></h5>\
              <span title="'+amazonObj[i]['rating']+' Reviews & Ratings" class="stars-container mt1 stars-'+(parseInt(amazonObj[i]['stars']*2)*10).toString()+' ml3">★★★★★</span>\
              <a target="_blank" href='+amazonObj[i]['url']+'><button class="button success mb1 price-btn"><strong>&#8377; '+amazonObj[i]['price']+'</strong></button></a>\
            </div>\
          </div>\
        ');
      }
      $(".amazon-blk .load").addClass("hide");
      $amazon_data_blk.fadeTo(500,1);
    }
  });

  //Snapdeal
  $.get(location.origin+"/search/snapdeal/"+$("#squery").val(),function(data,status,jqXHR){
    if(status === "success") {
      // console.log(data);
      var snapdealObj = sort ? _.sortBy(data.snapdeal, function(o){return o['price']}) : data.snapdeal,
      $snapdeal_data_blk = $("#search-results .snapdeal-blk .data-content");

      $snapdeal_data_blk.html("");
      for(i=0;i<snapdealObj.length&&i<maxResults;i++) {
        $snapdeal_data_blk.append('\
          <div class="row item-unit mb2">\
            <div class="columns medium-2 small-2 p1 text-center img-blk ml2">\
              <a target="_blank" href='+snapdealObj[i]['url']+'><img src='+snapdealObj[i]['image']+' /></a>\
            </div>\
            <div class="columns medium-9 small-9 pl0 info-blk">\
              <h5 class="mt1"><a target="_blank" href='+snapdealObj[i]['url']+'>'+snapdealObj[i]['title']+'</a></h5>\
              <span title="'+snapdealObj[i]['rating']+' Reviews & Ratings" class="stars-container mt1 stars-'+(parseInt(snapdealObj[i]['stars']*2)*10).toString()+' ml3">★★★★★</span>\
              <a target="_blank" href='+snapdealObj[i]['url']+'><button class="button success mb1 price-btn"><strong>&#8377; '+snapdealObj[i]['price']+'</strong></button></a>\
            </div>\
          </div>\
        ');
      }
      $(".snapdeal-blk .load").addClass("hide");
      $snapdeal_data_blk.fadeTo(500,1);
    }
  });

});

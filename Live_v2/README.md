Buy Sasta WebApp
------------------------

Pre-requisites
--------------

bower
pip

Getting Started
---------------
To get up and running, simply do the following:

    $ git clone https://ayushya@bitbucket.org/ayushya/sasta.git
    $ cd sasta

    # Install the requirements
    $ pip install -r requirements.txt

    # Install bower
    $ npm install -g bower
    $ bower install

    # Perform database migrations
    $ python manage.py makemigrations
    $ python manage.py migrate

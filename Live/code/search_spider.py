from bs4 import BeautifulSoup

import urllib2

def search_func(search_term):
    url="http://www.flipkart.com/search?q="+search_term
    content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content)
    lists=[]
    
    for x in soup.findAll("span", { "class" : "fk-font-17 fk-bold 11" }):
        lists.append(x.string)
    return lists


from flask import Flask, render_template,jsonify, request
import urllib2
from search_spider import search_func
import subprocess

 
app = Flask(__name__)     
 
@app.route('/')
def home():
  return render_template('home.html')

@app.route('/about')
def about():
  return render_template('about.html')

@app.route('/search/<query>', methods=['GET', 'POST'])
def search(query):
  # If the user browsed /hello/John, the output would be
  # "Hello John!
  result=search_func(query)
  t=', '.join(result)
  return t
 
if __name__ == '__main__':
  app.run(debug=False)

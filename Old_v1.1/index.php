<!DOCTYPE html>
<html>
<head>
	<title>Sasta</title>
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<script src="assets/js/jquery-2.1.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<div class="row">
		<div id="logo">
		<center><img  src="assets/img/logo.png"></center>
			
		</div>		
	</div>
	<div class="row">
	<form action="" method="post" onsubmit="addloader()">
	<div class="col-md-8 col-md-offset-2">
	<div class="search">
	
		<input type="text" class="form-control input-lg" name="squery" placeholder="Search for the Product here .. " required>	
	</div>
	</div>
	<div class="col-md-2">
	<div class="search">
	<button class="btn btn-lg btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
	</div>
	</div>
		</form>
	</div>
</div>



	<div id="load">
<?php
if(isset($_POST['squery'])){
	echo "<center><hr width=\"400\"><h2>Results</h2><h3>".$_POST['squery']."</h3><hr width='100'></center>";
$sq=$_POST['squery'];
$sqdb=$sq;
$sq=str_replace(' ', '+', $sq);
$url1 = 'http://www.flipkart.com/search?q='.$sq;
$url2 ='http://www.amazon.in/s/field-keywords='.$sq;
// $url3 ='http://www.snapdeal.com/search?keyword='.$sq.'&noOfResults=20';
// $url4 ='https://paytm.com/shop/search/?q='.$sq;


$ch = curl_init($url1);
$timeout = 5;

curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 10.10; ayushyajaiswal;) ayushya.org");
curl_setopt($ch, CURLOPT_FAILONERROR, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$html1 = curl_exec($ch);
curl_setopt($ch, CURLOPT_URL, $url2);
$html2 = curl_exec($ch);
curl_close($ch);

$itemname="pu-title";
$classname="fk-font-17";
    
    ?>
    <script type="text/javascript">
   	var q=<?php echo json_encode($sqdb) ?>;
    var site1=<?php echo json_encode($html1) ?>;
    var site2=<?php echo json_encode($html2) ?>;
    </script>
    <?php

$itemname="a-spacing-small";
$classname="s-price";
?>

<div class='container'>
<div class='row'>
<!-- Flipkart -->
<div class="col-md-6 rborder"><div class="row"><div class="col-md-4 col-md-offset-4"><h2>Flipkart</h2></div></div>
<br>
<div id="flipkart-data">
	
</div>
</div>

<!-- Amazon -->
<div class="col-md-6 rborder"><div class="row"><div class="col-md-4 col-md-offset-4"><h2>Amazon</h2></div></div>
<br>
<div id="amazon-data">
	
</div>
</div>


<script type="text/javascript">
	s1_tit=$(site1).find(".pu-title");
	s1_cnt=s1_tit.length;
	s1_pri=$(site1).find(".fk-font-17");
	s1_data="";
	for(i=0;i<s1_cnt;i++)
	{
		$("#flipkart-data").append('<div class="row rspace"><div class="col-md-8"><strong>'+$(s1_tit[i]).text().trim()+'</strong></div><div class="col-md-4"><a target="_blank" href="http://www.flipkart.com'+$(s1_tit[i]).find("a").attr("href")+'"><button class="btn btn-success">&#8377;&nbsp;'+$(s1_pri[i]).text().substr(3)+'</button></a></div></div>');
	}

	s2_tit=$(site2).find(".a-spacing-small");
	s2_cnt=s2_tit.length;
	s2_pri=$(site2).find(".s-price");
	s2_data="";
	for(i=0;i<s2_cnt-1;i++)
	{
		$("#amazon-data").append('<div class="row rspace"><div class="col-md-8"><strong>'+$(s2_tit[i+1]).text().trim()+'</strong></div><div class="col-md-4"><a target="_blank" href="'+$(s2_tit[i+1]).find("a").attr("href")+'"><button class="btn btn-success">&#8377;&nbsp;'+$(s2_pri[i]).text().trim()+'</button></a></div></div>');
	}
	</script>

</div> <!-- row -->
<br>
</div> <!-- container -->
<br>
<div class="container">

<div class="row" id="bottom">
	
</div>
	
</div>
<script type="text/javascript">
 var a;
	$.post("db.php",{

		query:q,
		f_price:$(s1_pri[0]).text().substr(3),
		a_price:$(s2_pri[0]).text().trim()

	},function(data, status){
        // alert("Data: " + data + "\nStatus: " + status);
        console.log(data);
        a=JSON.parse(data);
        $("#bottom").append("<h3>Last Seen Price</h3>" + "<div class='row'><div class='col-md-4 col-md-offset-4'><h4>&#8377;&nbsp;"+a[2]+"</h4></div><div class='col-md-4'><h4>&#8377;&nbsp;"+a[3]+"</h4></div></div><h5 style='text-align:right'>Last Updated On : "+ a[4] +"</h5>");
        


    });
</script>
<?php     
}
    ?>
  </div>
  <script type="text/javascript">
  function addloader () {
  	$("#load").prepend('<br><br><br><center><i class="fa fa-5x fa-refresh fa-spin"></i></center>')
  }
  </script>
</body>
</html>
<!DOCTYPE HTML>
<html>

<head>
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<script src="assets/js/jquery-2.1.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

  <script type="text/javascript">
  b=new Array();
  c=new Array();
  $.get("alldb.php",function(data,success,xhr){
  	// console.log(data);
  	n=JSON.parse(data);
  	a=JSON.parse(JSON.parse(data));
  	
	for( i=0;i<a.length;i++)
	{	
	a1=a[i][i+1][0];
	// alert(a[i][1][0]);
	a2=a[i][i+1][1];
	// console.log("a2 before 1 "+a2);
	a2=a2.replace(",","");
	// console.log("a2 after 1 "+a2);
	a2=a2.replace(",","");
	a2=parseInt(a2);
	b1=a[i][i+1][0];
	b2=a[i][i+1][2];
	b2=b2.replace(",","");
	b2=b2.replace(",","");
	b2=parseInt(b2);
	console.log("B before "+b[i]);
	b[i]={label:a1,y:a2};
	console.log("B after "+b[i].y);
	c[i]={label:b1,y:b2};
	
	}
      });
  window.onload = function () {
  	
    
setTimeout(function(){
	var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Price Comparison Of Flipkart and Amazon"
      },
      animationEnabled: true,
      axisY :{
        includeZero: false
      },
      toolTip: {
        shared: "true"
      },
      data: [
      {
        type: "spline",
        showInLegend: true,
        name: "Flipkart",
        markerSize: 0,
        dataPoints: b
      },
      {
        type: "spline",
        showInLegend: true,
        name: "Amazon",
        markerSize: 0,
        dataPoints: c
      }
      ]
    });

chart.render();
},200);


}
</script>

<script type="text/javascript" src="assets/js/canvasjs.min.js"></script>
</head>
<body>
  <center>
  <h1>Price Analysis</h1>
  </center>

  <br>
  <div class="container">
  	<div class="row">
  		<div id="chartContainer" style="height: 300px; width: 100%;">
  </div>
  	</div>
  </div>
</body>
</html>

-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2015 at 07:34 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sasta`
--
CREATE DATABASE IF NOT EXISTS `sasta` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sasta`;

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE IF NOT EXISTS `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) NOT NULL,
  `f_price` varchar(12) NOT NULL,
  `a_price` varchar(12) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `price`
--

INSERT INTO `price` (`id`, `product_name`, `f_price`, `a_price`, `last_update`) VALUES
(1, 'Test', '35000', '34000', '2015-05-07 21:19:32'),
(2, 'Test2', '32000', '31000', '2015-05-07 21:20:25'),
(3, 'Testing', '30000', '28000', '2015-05-07 21:20:55'),
(4, 'iphone 5s', ' 39,499', '35,960.00', '2015-04-30 11:09:08'),
(5, 'macbook pro', ' 59,734', '56,599.00', '2015-04-30 10:10:44'),
(6, 's4', ' 22,350', '16,499.00', '2015-05-07 18:15:30'),
(7, 'wd 1tb', ' 4,839', '4,112.00', '2015-05-08 04:07:28'),
(8, 'iphone 6s', ' 48,999', '34,889.00', '2015-05-08 04:10:04'),
(9, 'htc one m8', ' 39,750', '16,180.00', '2015-05-08 04:18:30'),
(10, 'wd 2tb', ' 7,078', '4,112.00', '2015-05-08 04:19:57'),
(11, 'lumia 640', ' 9,999', '7,336.00', '2015-05-08 04:26:07'),
(12, 'Test', '1253', '1254', '2015-05-08 04:53:12'),
(13, 'yu yureka', ' 139', '199.00', '2015-05-08 05:02:55');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

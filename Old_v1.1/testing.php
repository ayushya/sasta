<?php
 
$url = "http://www.flipkart.com/search?q=samsung+s5";
 
$response = getPriceFromFlipkart($url);
 
echo json_encode($response);
 
/* Returns the response in JSON format */
 
function getPriceFromFlipkart($url) {
 
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 10.10; ayushya;) ayushya.org");
	curl_setopt($curl, CURLOPT_FAILONERROR, true);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$html = curl_exec($curl);
	echo $html;
	curl_close($curl);
 
	$regex = '/<meta itemprop="price" content="([^"]*)"/';
	preg_match($regex, $html, $price);
 
	$regex = '/<h1[^>]*>([^<]*)<\/h1>/';
	preg_match($regex, $html, $title);
 
	$regex = '/data-src="([^"]*)"/i';
	preg_match($regex, $html, $image);
 
	if ($price && $title && $image) {
		$response = array("price" => "Rs. $price[1].00", "image" => $image[1], "title" => $title[1], "status" => "200");
	} else {
		$response = array("status" => "404", "error" => "We could not find the product details on Flipkart $url");
	}
 
	return $response;
}
 